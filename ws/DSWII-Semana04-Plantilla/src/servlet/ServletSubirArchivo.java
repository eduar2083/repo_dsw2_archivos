package servlet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import entidad.Experiencia;
import entidad.Postulante;
import model.modelExperiencia;

@WebServlet("/ServletSubirArchivo")
@MultipartConfig(maxFileSize = 1024 * 1024 * 5, maxRequestSize = 1024 * 1024 * 20)
public class ServletSubirArchivo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private modelExperiencia mExperiencia = new modelExperiencia();

	public ServletSubirArchivo() {
		super();
	}

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int op = Integer.parseInt(request.getParameter("op"));

		switch (op) {
		case 1:
			registro(request, response);
			break;
		}
	}

	private void registro(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int codigo;
		String mensaje;
		
		// Obtener el dni
		String dni = request.getParameter("dni");
		
		// Verificar si el dni ya existe
		Postulante p = mExperiencia.findPostulanteXDni(dni);
		
		if (p != null) {
			codigo = -1;
			mensaje = "El dni " + dni + " ya est� registrado";
		}
		else {
			List<Experiencia> listaExpProfesional = guardarArchivosExpProfesional(request);
			List<Experiencia> listaExpDocente = guardarArchivosExpDocente(request);
			
			if (listaExpProfesional.size() == 0 && listaExpDocente.size() == 0) {
				codigo = -1;
				mensaje = "Debe adjuntar al menos un documento";
			}
			else {
				// Obtener datos del postulante
				String nombre = request.getParameter("nombre");
				String paterno = request.getParameter("paterno");
				String materno = request.getParameter("materno");
				
				// Crear la entidad Postulante
				p = new Postulante();
				p.setPaterno(paterno);
				p.setMaterno(materno);
				p.setNombre(nombre);
				p.setDni(dni);
				
				int r = mExperiencia.addExperiencia(p, listaExpProfesional, listaExpDocente);
				
				if (r == 0) {
					codigo = 0;
					mensaje = "Registrado correctamente";
				}
				else {
					codigo = -1;
					mensaje = "Ocurri� un error";
				}
			}
		}
		
		request.getSession().setAttribute("codigo", codigo);
		request.getSession().setAttribute("mensaje", mensaje);
		request.getRequestDispatcher("/registroPostulante.jsp").forward(request, response);
		
	}
	
	private List<Experiencia> guardarArchivosExpProfesional(HttpServletRequest request) throws IOException, ServletException {
		List<Experiencia> lista = new ArrayList<Experiencia>();
		
		// Obtener el dni
		String dni = request.getParameter("dni");

		// Obtener nombres de las empresas
		String[] empresas = request.getParameterValues("empresaProfesional");
		
		// Obtener la ruta del servidor
		String ruta = getServletContext().getRealPath("") + "subir\\exp-profesional\\" + dni + "\\";

		// Obtener los archivos seleccionados del grupo "archivoProfesional"
		List<Part> fileParts = request.getParts().stream().filter(part -> "archivoProfesional".equals(part.getName()))
				.collect(Collectors.toList());

		// Crear la carpeta contenedora
		File carpeta = new File(ruta);
		carpeta.mkdirs();
		
		// Contador
		int pos = 0;

		// Recorrer los archivos
		for (Part filePart : fileParts) {

			// Obtener el nombre del archivo
			String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
			
			// Si no hay archivo, continuar con el siguiente elemento de la lista
			if (fileName.equals(""))
				continue;

			// Obtener el archivo binario
			InputStream is = filePart.getInputStream();
			int size = is.available();
			byte[] bytes = new byte[size];

			// Leer el archivo binario
			is.read(bytes);

			// Nombre del archivo a crear
			File file = new File(ruta + fileName);

			// Archivo de salida en binario
			FileOutputStream fos = new FileOutputStream(file);

			// Escribir sobre el flujo
			fos.write(bytes);

			// Cerrar el flujo
			fos.close();
			
			Experiencia exp = new Experiencia();
			
			// Establecer dni
			exp.setDni(dni);
			
			// Establecer la empresa
			exp.setEmpresa(empresas[pos++]);
			
			// Establecer nombre del archivo
			exp.setNombreArchivo(fileName);
			
			// Establecer el archivo correspondiente
			exp.setArchivo(bytes);
			
			// Establecer ruta
			exp.setRuta("subir/exp-profesional/" + dni + "/" + fileName);
			
			// Agregar experiencia a la lista
			lista.add(exp);
		}
		
		return lista;
	}
	
	private List<Experiencia> guardarArchivosExpDocente(HttpServletRequest request) throws IOException, ServletException {
		List<Experiencia> lista = new ArrayList<Experiencia>();
		
		// Obtener el dni
		String dni = request.getParameter("dni");

		// Obtener nombres de las empresas
		String[] empresas = request.getParameterValues("empresaDocente");
		
		// Obtener la ruta del servidor
		String ruta = getServletContext().getRealPath("") + "subir\\exp-docente\\" + dni + "\\";

		// Obtener los archivos seleccionados del grupo "archivoProfesional"
		List<Part> fileParts = request.getParts().stream().filter(part -> "archivoDocente".equals(part.getName()))
				.collect(Collectors.toList());

		// Crear la carpeta contenedora
		File carpeta = new File(ruta);
		carpeta.mkdirs();
		
		// Contador
		int pos = 0;

		// Recorrer los archivos
		for (Part filePart : fileParts) {

			// Obtener el nombre del archivo
			String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
			
			// Si no hay archivo, continuar con el siguiente elemento de la lista
			if (fileName.equals(""))
				continue;

			// Obtener el archivo binario
			InputStream is = filePart.getInputStream();
			int size = is.available();
			byte[] bytes = new byte[size];

			// Leer el archivo binario
			is.read(bytes);

			// Nombre del archivo a crear
			File file = new File(ruta + fileName);

			// Archivo de salida en binario
			FileOutputStream fos = new FileOutputStream(file);

			// Escribir sobre el flujo
			fos.write(bytes);

			// Cerrar el flujo
			fos.close();
			
			Experiencia exp = new Experiencia();
			
			// Establecer dni
			exp.setDni(dni);
			
			// Establecer la empresa
			exp.setEmpresa(empresas[pos++]);
			
			// Establecer nombre del archivo
			exp.setNombreArchivo(fileName);
			
			// Establecer el archivo correspondiente
			exp.setArchivo(bytes);
			
			// Establecer ruta
			exp.setRuta("subir/exp-docente/" + dni + "/" + fileName);
			
			// Agregar experiencia a la lista
			lista.add(exp);
		}
		
		return lista;
	}
}
