package servlet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entidad.Experiencia;
import entidad.Postulante;
import model.modelExperiencia;

@WebServlet("/ServletListado")
public class ServletListado extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private modelExperiencia mExperiencia = new modelExperiencia();
       
    public ServletListado() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int op = Integer.parseInt(request.getParameter("op"));
		
		switch(op) {
		case 1:
			listadoGeneral(request, response);
			break;
		case 2:
			detalleExperiencia(request, response);
			break;
		case 3:
			descargar(request, response);
			break;
		}
	}
    
    private void listadoGeneral(HttpServletRequest request, HttpServletResponse response) throws IOException {
    	String dni = request.getParameter("dni");
    	String nombre = request.getParameter("nombre");
    	
    	// Invocar el sp
    	List<Postulante> lista = mExperiencia.BuscarPostulantes(dni, nombre);
    	
    	JsonArrayBuilder array = Json.createArrayBuilder();
    	for (Postulante p : lista) {
    		JsonObject obj = Json.createObjectBuilder()
    							.add("dni", p.getDni())
    							.add("nombreCompleto", p.getNombreCompleto())
    							.build();
    		
    		array.add(obj);
    	}
    	
    	response.setContentType("application/json;charset=utf8");
    	PrintWriter salida = response.getWriter();
    	salida.print(array.build());
    }

	private void detalleExperiencia(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// Obtener el dni
		String dni = request.getParameter("dni");
		
		// Obtener el tipo de listado (1=Profesional, 2=Docente)
		int tipo = Integer.parseInt(request.getParameter("tipo"));
		
		// Obtener datos personales del postulante
		Postulante p = mExperiencia.findPostulanteXDni(dni);
		
		// Construmos un objeto Json para el postulante
		JsonObject jsonPostulante = Json.createObjectBuilder()
										.add("dni", p.getDni())
										.add("nombre", p.getNombre())
										.add("paterno", p.getPaterno())
										.add("materno", p.getMaterno())
										.build();
		
		// Obtener experiencia Profesional del Postulante
		List<Experiencia> lista = mExperiencia.listExperienciaXDni(dni, tipo);
		
		// Construimos un arreglo Json para el listado de experiencia profesional
		JsonArrayBuilder array = Json.createArrayBuilder();
		for (Experiencia e : lista) {
			JsonObject obj = Json.createObjectBuilder()
								.add("dni", e.getDni())
								.add("empresa", e.getEmpresa())
								.add("nombre", e.getNombreArchivo())
								.add("ruta", e.getRuta())
								.build();
			
			array.add(obj);
		}
		
		// Construimos un objetos json complejo
		JsonObject obj = Json.createObjectBuilder()
								.add("postulante", jsonPostulante)
								.add("listaExperiencia", array)
								.build();
		
		response.setContentType("application/json;charset=UTF8");
		PrintWriter salida = response.getWriter();
		salida.print(obj);
	}
	
	private void descargar(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// Recuperar datos de la p�gina
		String dni = request.getParameter("dni");
		String empresa = request.getParameter("empresa");
		String nombreArchivo = request.getParameter("nombreArchivo");
		
		// Obtener ruta del servidor
		String rutaExpProfesional = getServletContext().getRealPath("") + "\\descargas\\exp-profesional\\" + dni + "\\";
		
		File carpetaDni = new File(rutaExpProfesional);
		carpetaDni.mkdirs();
		
		// Buscar el archivo seg�n los datos (dni, empresa y nombreArchivo)
		Experiencia entidad = mExperiencia.findExperiencia(dni, empresa, nombreArchivo);
		
		// Crear el archivo f�sico
		File file = new File(rutaExpProfesional + nombreArchivo);
		FileOutputStream fos = new FileOutputStream(file);
		fos.write(entidad.getArchivo());
		
		// Crear un objeto JSON
		JsonObject json = Json.createObjectBuilder().add("ruta", "descargas/exp-profesional/" + dni + "/" + entidad.getNombreArchivo()).build();
		
		response.setContentType("application/json;charset=utf-8");
		PrintWriter salida = response.getWriter();
		salida.print(json);
		
		fos.close();
	}
}
