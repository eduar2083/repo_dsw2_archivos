package model;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import entidad.Experiencia;
import entidad.Postulante;
import utils.MysqlDBConexion;

public class modelExperiencia {
	public List<Postulante> BuscarPostulantes(String dni, String nombre) {
		List<Postulante> lista = new ArrayList<Postulante>();
		
		Connection cn = null;
		CallableStatement cstm = null;
		ResultSet rs = null;
		
		try {
			cn = MysqlDBConexion.getConexion();
			String sql = "call usp_buscar_postulante(?,?)";
			cstm = cn.prepareCall(sql);
			
			if (dni != null)
				cstm.setString(1, dni);
			else
				cstm.setNull(1, java.sql.Types.NULL);
			
			if (nombre != null)
				cstm.setString(2, nombre);
			else
				cstm.setNull(2, java.sql.Types.NULL);
			
			rs = cstm.executeQuery();
			
			Postulante entidad = null;
			while (rs.next()) {
				entidad = new Postulante();
				entidad.setIdPostulante(rs.getInt(1));
				entidad.setDni(rs.getString(2));
				entidad.setNombreCompleto(rs.getString(3));
				
				lista.add(entidad);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				if (rs != null) rs.close();
				if (cstm != null) cstm.close();
				if (cn != null) cn.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		
		return lista;
	}
	
	public int addExperiencia(Experiencia e){
		int estado=-1;
		Connection cn=null;
		PreparedStatement pstm=null;
		try {
			cn=MysqlDBConexion.getConexion();
			String sql="insert into tb_experiencia_profesional values(?,?,?,?,?)";
			pstm=cn.prepareStatement(sql);
			pstm.setString(1,e.getDni());
			pstm.setString(2,e.getEmpresa());
			pstm.setString(3,e.getNombreArchivo());
			pstm.setBytes(4,e.getArchivo());
			pstm.setString(5, e.getRuta());
			estado=pstm.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();	
		}
		finally{
			try {
				if(pstm!=null) pstm.close();
				if(cn!=null) cn.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return estado;
	}
	
	public int addExperiencia(Postulante p, List<Experiencia> listaExpProfesional, List<Experiencia> listaExpDocente){
		int estado = -1;
		Connection cn = null;
		PreparedStatement pstm = null;
		
		try {
			cn=MysqlDBConexion.getConexion();
			
			// Anular el auto env�o
			cn.setAutoCommit(false);
			
			// Guardar el postulante
			String sql = "insert into tb_postulante values(null,?,?,?,?)";
			pstm=cn.prepareStatement(sql);
			pstm.setString(1, p.getNombre());
			pstm.setString(2, p.getPaterno());
			pstm.setString(3, p.getMaterno());
			pstm.setString(4, p.getDni());
			estado=pstm.executeUpdate();
			
			for (Experiencia e : listaExpProfesional) {
				sql="insert into tb_experiencia_profesional values(?,?,?,?,?)";
				pstm=cn.prepareStatement(sql);
				pstm.setString(1,e.getDni());
				pstm.setString(2,e.getEmpresa());
				pstm.setString(3,e.getNombreArchivo());
				pstm.setBytes(4,e.getArchivo());
				pstm.setString(5, e.getRuta());
				estado=pstm.executeUpdate();
			}
			
			for (Experiencia e : listaExpDocente) {
				sql="insert into tb_experiencia_docente values(?,?,?,?,?)";
				pstm=cn.prepareStatement(sql);
				pstm.setString(1,e.getDni());
				pstm.setString(2,e.getEmpresa());
				pstm.setString(3,e.getNombreArchivo());
				pstm.setBytes(4,e.getArchivo());
				pstm.setString(5, e.getRuta());
				estado=pstm.executeUpdate();
			}
			
			// Confirmar cambios
			cn.commit();
			
			estado = 0;
		} catch (Exception ex) {
			try {
				if (cn != null) cn.rollback();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		finally{
			try {
				if(pstm!=null) pstm.close();
				if(cn!=null) cn.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return estado;
	}
	
	public Experiencia findExperiencia(String dni,String empresa,String nombre){
		Experiencia exp=new Experiencia();
		Connection cn=null;
		PreparedStatement pstm=null;
		ResultSet rs=null;
		try {
			cn=MysqlDBConexion.getConexion();
			String sql="SELECT * FROM tb_experiencia_profesional where dni_pos=? and nom_empresa=? and nom_archivo=?";
			pstm=cn.prepareStatement(sql);
			pstm.setString(1, dni);
			pstm.setString(2, empresa);
			pstm.setString(3, nombre);
			rs=pstm.executeQuery();
			if(rs.next()) {
				exp.setDni(rs.getString(1));
				exp.setEmpresa(rs.getString(2));
				exp.setNombreArchivo(rs.getString(3));
				exp.setArchivo(rs.getBytes(4));
			}
		} catch (Exception e) {
			e.printStackTrace();	
		}
		finally{
			try {
				if(rs!=null) rs.close();
				if(pstm!=null) pstm.close();
				if(cn!=null) cn.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return exp;
	}
	
	public Postulante findPostulanteXDni(String dni) {
		Postulante p = null;
		
		Connection cn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			cn=MysqlDBConexion.getConexion();
			String sql = "SELECT * FROM tb_postulante where dni=?";
			pstm = cn.prepareStatement(sql);
			pstm.setString(1, dni);
			rs=pstm.executeQuery();
			
			if(rs.next()) {
				p = new Postulante();
				p.setIdPostulante(rs.getInt(1));
				p.setNombre(rs.getString(2));
				p.setPaterno(rs.getString(3));
				p.setMaterno(rs.getString(4));
				p.setDni(rs.getString(5));
			}
		} catch (Exception e) {
			e.printStackTrace();	
		}
		finally{
			try {
				if(rs!=null) rs.close();
				if(pstm!=null) pstm.close();
				if(cn!=null) cn.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return p;
	}
	
	public List<Experiencia> listExperienciaXDni(String dni, int tipo){
		List<Experiencia> lista=new ArrayList<Experiencia>();
		Experiencia exp;
		Connection cn=null;
		PreparedStatement pstm=null;
		ResultSet rs=null;
		try {
			cn=MysqlDBConexion.getConexion();
			String sql;
			if (tipo == 1)
				sql = "SELECT * FROM tb_experiencia_profesional where dni_pos=?";
			else
				sql = "SELECT * FROM tb_experiencia_docente where dni_pos=?";
			pstm=cn.prepareStatement(sql);
			pstm.setString(1, dni);
			rs=pstm.executeQuery();
			while(rs.next()){
				exp=new Experiencia();
				exp.setDni(rs.getString(1));
				exp.setEmpresa(rs.getString(2));
				exp.setNombreArchivo(rs.getString(3));
				//exp.setArchivo(rs.getBytes(4));
				exp.setRuta(rs.getString(5));
				lista.add(exp);
			}
		} catch (Exception e) {
			e.printStackTrace();	
		}
		finally{
			try {
				if(rs!=null) rs.close();
				if(pstm!=null) pstm.close();
				if(cn!=null) cn.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return lista;
	}
}
