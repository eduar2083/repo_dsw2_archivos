<!DOCTYPE html>
<html lang="esS">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<link rel="stylesheet" href="css/bootstrap.css" />


<title>Postulante</title>
<style>
.modal-header, h4, .close {
	background-color: #286090;
	color: white !important;
	text-align: center;
	font-size: 20px;
}

.glyphicon-download-alt {
	font-size: 20px;
}

.glyphicon-download-alt:hover {
	cursor: pointer;
}
</style>
</head>
<body>
	<!-- INICIO DIV container -->
	<div class="container">
		<div class="modal-dialog" style="width: 90%">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="padding: 5px 20px">
					<h4>Registro de Postulante</h4>
				</div>
				<div class="modal-body" style="padding: 20px 10px;">
					<div class="panel-group" id="steps">

						<div class="panel panel-default">
							<div class="panel-heading"
								style="text-align: center; font-size: 14px">Datos
								Personales</div>
							<div class="panel-body">
								<div class="form-group col-sm-12">
									<div class="form-group col-sm-6">
										<div>Nombre</div>
										<input class="form-control" type="text" name="nombre" id="nombre" />
									</div>
									<div class="form-group col-sm-6">
										<div>Paterno</div>
										<input class="form-control" type="text" name="paterno" id="paterno" />
									</div>
								</div>
								<div class="form-group col-sm-12">
									<div class="form-group col-sm-6">
										<div>Materno</div>
										<input class="form-control" type="text" name="materno" id="materno" />
									</div>
									<div class="form-group col-sm-6">
										<div>DNI</div>
										<input class="form-control" type="text" name="dni" id="dni" />
									</div>
								</div>
								<div class="form-group col-sm-12">
									<a href="listaGeneral.jsp" class="btn btn-danger">Volver a listado general</a>
								</div>
							</div>
						</div>
					</div>
					<div class="panel-group" id="steps">

						<div class="panel panel-default">
							<div class="panel-heading" style="text-align: center; font-size: 14px">
								Experiencia Docente</div>
							<br>
							<table id="id_table" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Dni</th>
										<th>Empresa</th>
										<th>Nombre</th>
										<th>Acci�n</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- FIN DIV container -->



	<script type="text/javascript">
$(document).ready(function() {
	listar();
	
	// Desactivar autocomplete
	$('input[type=text]').attr('autocomplete', 'off');
})

function listar(){
	// Obtener el dni seleccionado
	var dni = window.sessionStorage.getItem('dniSelect');
	
	$.getJSON("ServletListado", {op: 2, dni: dni, tipo: 2}, function(data) {
		var p = data.postulante;
		var listaExperiencia = data.listaExperiencia;
		
		// Pintar datos del postulante
		$('#nombre').val(p.nombre);
		$('#paterno').val(p.paterno);
		$('#materno').val(p.materno);
		$('#dni').val(p.dni);
		
		listaExperiencia.map(function(e, i) {
			var enlaceDescarga = "<a href='" + e.ruta + "' target='_blank' class='btn btn-default'><span class='glyphicon glyphicon-download-alt'></span></a>";
			
			$('#id_table tbody').append('<tr><td>' + e.dni + "</td><td>" + e.empresa + "</td><td>" + e.nombre + "</td><td>" + enlaceDescarga + "</td></tr>");
		});
	});
}
</script>

</body>
</html>
