<!DOCTYPE html>
<html lang="esS">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<link rel="stylesheet" href="css/toastr.min.css" />
<script type="text/javascript" src="js/toastr.min.js"></script>

<link rel="stylesheet" href="css/bootstrap.css" />


<title>Postulante</title>
<style>
.modal-header, h4, .close {
	background-color: #286090;
	color: white !important;
	text-align: center;
	font-size: 20px;
}

.glyphicon-download-alt {
	font-size: 20px;
}

.glyphicon-download-alt:hover {
	cursor: pointer;
}
</style>
</head>
<body>

	<!-- INICIO DIV container -->
	<div class="container">
		<div class="modal-dialog" style="width: 90%">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="padding: 5px 20px">
					<h4>Listado de Postulantes</h4>
				</div>
				<div class="modal-body" style="padding: 20px 10px;">
					<div class="panel-group" id="steps">
						<div class="row" style="margin-bottom:5px">
							<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="Filtar por dni" name="dni" id="dni" maxlength="8" />
							</div>
							<div class="col-sm-4">
								<input type="text" class="form-control" placeholder="Filtrar por nombre" name="nombre" id="nombre" maxlength="50" />
							</div>
							<div class="col-sm-4">
								<a href="registroPostulante.jsp" class="btn btn-primary">Nuevo postulante</a>
							</div>
						</div>
						<div class="panel panel-default">
							<table id="id_table" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Dni</th>
										<th>Nombre completo</th>
										<th class='text-center'>Acci�n</th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- FIN DIV container -->



	<script type="text/javascript">
$(document).ready(function() {
	listar();
	
	// Desactivar autocomplete
	$('input[type=text]').attr('autocomplete', 'off');
})
function listar(){
	// Obtener datos de filtrado
	var dni = $.trim($('#dni').val());
	var nombre = $.trim($('#nombre').val());
	
	$.getJSON("ServletListado", {op:1, dni: dni, nombre: nombre}, function(data) {
		$('#id_table tbody').html('');
		data.map(function(e, i) {
			var enlaceExpProfesional = "<button type='button' id='idVerProfesional' title='ver experiencia profesional' class='btn btn-default mr-2'><span class='glyphicon glyphicon-user'></span></button>";
			var enlaceExpDocente = "<button type='button' id='idVerDocente' title='ver experiencia docente' class='btn btn-default'><span class='glyphicon glyphicon-search'></span></button>";
			$('#id_table tbody').append('<tr><td>' + e.dni + "</td><td>" + e.nombreCompleto + "</td><td class='text-center'>" + enlaceExpProfesional + enlaceExpDocente + "</td></tr>");
		});
	});
}

$('#dni,#nombre').bind('keyup', listar);

$(document).on('click', '#idVerProfesional', function() {
	// Obtener el dni seleccionado
	var dni = $(this).parents('tr').find('td').get(0).innerText;
	
	// Guardar el dni seleccionado
	window.sessionStorage.setItem('dniSelect', dni);
	
	// Redireccionar la p�gia lista1
	window.location.href = 'lista1.jsp';
});

$(document).on('click', '#idVerDocente', function() {
	// Obtener el dni seleccionado
	var dni = $(this).parents('tr').find('td').get(0).innerText;
	
	// Guardar el dni seleccionado
	window.sessionStorage.setItem('dniSelect', dni);
	
	// Redireccionar la p�gia lista1
	window.location.href = 'lista2.jsp';
});
</script>


</body>
</html>
