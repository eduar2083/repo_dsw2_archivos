<!DOCTYPE html>
<html lang="esS">
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<link rel="stylesheet" href="css/bootstrap.css" />
<link rel="stylesheet" href="css/bootstrap-theme.min.css" />
<link rel="stylesheet" href="css/bootstrapValidator.css" />
<script type="text/javascript" src="js/bootstrapValidator.js"></script>

<link rel="stylesheet" href="css/toastr.min.css" />
<script type="text/javascript" src="js/toastr.min.js"></script>

<title>Postulante</title>
<style>
.modal-header, h4, .close {
	background-color: #286090;
	color: white !important;
	text-align: center;
	font-size: 20px;
}
</style>

</head>
<body>

	<!-- INICIO DIV NUEVO -->
	<div id="idModalRegistra">
		<div class="modal-dialog" style="width: 90%">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header" style="padding: 5px 20px">
					<h4>Registro de Postulante</h4>
				</div>
				<div class="modal-body" style="padding: 20px 10px;">
					<c:if test="${sessionScope.codigo != null }">
						<!--<div class="alert alert-warning fade in" id="success-alert">
							<a href="#" class="close" data-dismiss="alert">&times;</a> <strong>Sistema:</strong>
							${sessionScope.mensaje}
						</div> -->
						<input type="hidden" value="${sessionScope.codigo}" id="hdCodigo" />
						<input type="hidden" value="${sessionScope.mensaje}" id="hdMensaje" />
					</c:if>
					<c:remove var="codigo" />
					<c:remove var="mensaje" />
					<form id="idRegistra" action="ServletSubirArchivo?op=1" method="post"
						enctype="multipart/form-data" class="popup-form">
						<div class="panel-group" id="steps">

							<div class="panel panel-default">
								<div class="panel-heading"
									style="text-align: center; font-size: 14px">Datos
									Personales</div>
								<div class="panel-body">
									<div class="form-group col-sm-12">
										<div class="form-group col-sm-6">
											<div>Nombre</div>
											<input class="form-control" type="text" name="nombre" id="nombre" />
										</div>
										<div class="form-group col-sm-6">
											<div>Paterno</div>
											<input class="form-control" type="text" name="paterno" id="paterno" />
										</div>
									</div>
									<div class="form-group col-sm-12">
										<div class="form-group col-sm-6">
											<div>Materno</div>
											<input class="form-control" type="text" name="materno" id="materno" />
										</div>
										<div class="form-group col-sm-6">
											<div>DNI</div>
											<input class="form-control" type="text" name="dni" id="dni" />
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="panel-group" id="steps">
							<div class="panel panel-default">
								<div class="panel-heading text-center">
									Experiencia Profesional
								</div>
								<div class="panel-body" id="adicionarExperienciaProfesional">
									<div class="form-group col-sm-12">
										<div class="form-group col-sm-4">
											<div>Empresa</div>
											<input class="form-control" type="text" name="empresaProfesional" />
										</div>
										<div class="form-group col-sm-4">
											<div>Archivo</div>
											<input class="form-control" type="file" name="archivoProfesional" />
										</div>
										<div class="form-group col-sm-4" style="text-align: right">
											<button id='btnAdicionarExpProfesional' type='button' class='btn btn-primary'><span class='glyphicon glyphicon-plus'></span></button>
										</div>
									</div>

								</div>
							</div>
						</div>

						<div class="panel-group" id="steps">
							<div class="panel panel-default">
								<div class="panel-heading text-center">
									Experiencia Docente
								</div>
								<div class="panel-body" id="adicionarExperienciaDocente">
									<div class="form-group col-sm-4">
										<div>Empresa</div>
										<input class="form-control" type="text" name="empresaDocente" />
									</div>
									<div class="form-group col-sm-4">
										<div>Archivo</div>
										<input class="form-control" type="file" name="archivoDocente" />
									</div>
									<div class="form-group col-sm-4" style="text-align: right">
										<button id='btnAdicionarExpDocente' type='button' class='btn btn-primary'><span class='glyphicon glyphicon-plus'></span></button>
									</div>
								</div>
							</div>
							<div class="modal-footer" style="padding: 5px 20px; text-align: center">
								<button type="submit" class="btn btn-primary" id="btnRegistrar">REGISTRAR</button>
								<a href="listaGeneral.jsp" class="btn btn-danger">Volver al listado general</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- FIN DIV NUEVO -->

	<script type="text/javascript">
		$(function() {
			$('#idRegistra').bootstrapValidator({
				feedbackIcons: {
		            valid: 'glyphicon glyphicon-ok',
		            invalid: 'glyphicon glyphicon-remove',
		            validating: 'glyphicon glyphicon-refresh'
		        },
		        fields: {
		        	nombre: {
		        		validators: {
		        			notEmpty:{
								message: 'Ingresar nombre',
							},
							regexp:{
								regexp:/^[a-zA-Z\s\�\�\�\�\s\�\�\�\�\�\�\�\�]+$/,
								message:'solo letras'
							},
							stringLength: {
		                        max: 50,
		                        message: 'M�ximo 50 caracteres'
		                    }
		        		}
		        	},
		        	paterno: {
		        		validators: {
		        			notEmpty:{
								message: 'Ingresar apellido paterno',
							},
							regexp:{
								regexp:/^[a-zA-Z\s\�\�\�\�\s\�\�\�\�\�\�\�\�]+$/,
								message:'solo letras'
							},
							stringLength: {
		                        max: 50,
		                        message: 'M�ximo 50 caracteres'
		                    }
		        		}
		        	},
		        	materno: {
		        		validators: {
		        			regexp:{
								regexp:/^[a-zA-Z\s\�\�\�\�\s\�\�\�\�\�\�\�\�]+$/,
								message:'solo letras'
							},
							stringLength: {
		                        max: 50,
		                        message: 'M�ximo 50 caracteres'
		                    }
		        		}
		        	},
		        	dni: {
		        		validators: {
		        			notEmpty:{
								message: 'Ingresar dni',
							},
		        			regexp:{
								regexp: /^[0-9]{8}/,
								message:'Debe ingresar 8 digitos'
							}
		        		}
		        	}
		        }
			});
			
			// Desactivar autocomplete
			$('input[type=text]').attr('autocomplete', 'off');
		});
	
		$('#btnAdicionarExpProfesional').bind('click', function() {
			var fila = $("<div class='form-group col-sm-12 divExperiencia'>"
					+ "<div class='form-group col-sm-4'>"
					+ "<label>Empresa</label>"
					+ "<input class='form-control' type='text' name='empresaProfesional'/>"
					+ "</div>"
					+ "<div class='form-group col-sm-4'>"
					+ "<label>Archivo</label>"
					+ "<input class='form-control' type='file' name='archivoProfesional'/>"
					+ "</div>"
					+ "<div class='form-group col-sm-4'>"
					+ "<div style='text-align: right'>"
					+ "<div>Acci�n</div>"
					+ "<button type='button' class='btn btn-danger' id='btnEliminar'>Eliminar</button>"
					+ "</div>" + "</div>" + "</div>");

			$('#adicionarExperienciaProfesional').append(fila);
			fila.hide().show(750);
		});
		
		$('#btnAdicionarExpDocente').bind('click', function() {
			var fila = $("<div class='form-group col-sm-12 divExperiencia'>"
					+ "<div class='form-group col-sm-4'>"
					+ "<label>Empresa</label>"
					+ "<input class='form-control' type='text' name='empresaDocente'/>"
					+ "</div>"
					+ "<div class='form-group col-sm-4'>"
					+ "<label>Archivo</label>"
					+ "<input class='form-control' type='file' name='archivoDocente'/>"
					+ "</div>"
					+ "<div class='form-group col-sm-4'>"
					+ "<div style='text-align: right'>"
					+ "<div>Acci�n</div>"
					+ "<button type='button' class='btn btn-danger' id='btnEliminar'>Eliminar</button>"
					+ "</div>" + "</div>" + "</div>");

			$('#adicionarExperienciaDocente').append(fila);
			fila.hide().show(750);
		});
		
		$(document).on('click', '#btnEliminar', function() {
			$(this).parents('.divExperiencia').hide(750, function() {
				this.remove();
			})
		});

		/*
		$("#success-alert").fadeTo(2000, 1500).slideUp(1500, function() {
			$("#success-alert").slideUp(1500);
			window.location.href = "registroPostulante.jsp";
		});
		*/
		
		var codigo = $('#hdCodigo').val();
		var mensaje = $('#hdMensaje').val();
		if (codigo != null) {
			toastr.options.onHidden = function() {
				// Redirecci�n al listado s�lo en caso de �xito
				if (codigo == 0) {
					window.location.href = "listaGeneral.jsp";
				}
			}
			switch (codigo) {
			case "0":
				toastr.success(mensaje);
				break;
			case "-1":
				toastr.error(mensaje);
				break;
			}
			
		}
		
	</script>

</body>
</html>












